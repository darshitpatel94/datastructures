package darshit.patel.dfs.linkedlist;

import java.util.Iterator;

import darshit.patel.dfs.linkedlist.singly.SinglyLinkedList;

public class TestLinkList {

	public static void main(String[] args) {
		
		SinglyLinkedList<String> ll = new SinglyLinkedList<String>();
		ll.addFirst("1");
		ll.addFirst("2");
		ll.addFirst("3");
		ll.addFirst("4");
		ll.addFirst("5");
		ll.printLinkedList();
		System.out.println(ll.indexOf("4"));
		System.out.println(ll.indexOf("3"));
		System.out.println(ll.indexOf("2"));
		System.out.println(ll.indexOf("21"));
		System.out.println("===========================");
		
		Iterator<String> it = ll.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		System.out.println(ll.toString());
		
	}
	
}
