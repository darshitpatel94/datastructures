package darshit.patel.dfs.linkedlist.iface;

public interface LinkedList<T> extends Iterable<T>{
	
	public void clear();
	
	public int size();
	
	public boolean isEmpty();
	
	public void addFirst(T data);
	
	public void addLast(T data);
	
	public T peekFirst();
	
	public T peekLast();
	
	public void removeFirst();
	
	public void removeLast();
	
	public boolean removeAt(int index);
	
	public boolean remove(T data);
	
	public int indexOf(T data);
	
	public boolean contains(T data);
	
}
