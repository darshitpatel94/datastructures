package darshit.patel.dfs.linkedlist.singly;

import java.util.Iterator;

import darshit.patel.dfs.linkedlist.Node;
import darshit.patel.dfs.linkedlist.iface.LinkedList;

public class SinglyLinkedList<T> implements LinkedList<T>{

	private int size = 0;
	private Node<T> head = null;

	public void clear() {
		Node<T> pointerNode = head;
		while(pointerNode != null) {
			Node<T> nextNode = pointerNode.next;
			pointerNode.data = null;
			pointerNode.next = null;
			pointerNode = nextNode;
		}
		head = pointerNode = null;
		size = 0;
	}

	public int size() {return size;}

	public boolean isEmpty() { return 0 == size();}

	public void addFirst(T data) {
		if(size() == 0) {
			head = new Node<T>(data, null);
		}else {
			Node<T> headNode = new Node<T>(data, head);
			head = headNode;
		}
		size++;
	}

	public void addLast(T data) {

		Node<T> traversalNode = head;
		Node<T> tailNode = null;
		Node<T> lastNode = new Node<T>(data, null);

		if(isEmpty()) {
			head = lastNode;
		}else {
			// here we traversal node we only use for traversing, however, we can 
			// use it as tailNode, too. I am creating a separate node 
			//called as TailNode
			while(traversalNode != null) {
				tailNode = traversalNode;
				traversalNode = traversalNode.next;
			}
			tailNode.next = lastNode;
			size++;
		}

	}

	public T peekFirst() {
		if(isEmpty()) {	throw new RuntimeException("Empty List");}
		return head.data;
	}

	public T peekLast() {
		if(isEmpty()) {	throw new RuntimeException("Empty List");}
		Node<T> traversalNode = head;
		Node<T> tailNode = null;
		while(traversalNode.next != null) {
			tailNode = traversalNode;
			traversalNode = traversalNode.next;
		}
		return tailNode.data;
	}

	public void removeFirst() {
		if(isEmpty()) { throw new RuntimeException("Empty List"); }
		Node<T> newHeadNode = head.next;
		head = null;
		head = newHeadNode;
		size--;
	}

	public void removeLast() {
		if(isEmpty()) { throw new RuntimeException("Empty List"); }
		Node<T> traversalNode = head;
		Node<T> tailNode = null;
		while(traversalNode.next != null) {
			tailNode = traversalNode;
			traversalNode = traversalNode.next;
		}
		traversalNode.data = null;
		traversalNode = null;
		tailNode.next = null;
		size--;
	}

	public boolean removeAt(int index) {
		if(index < 0 || index >= size)throw new IllegalArgumentException("Illegal index to remove element");
		if(index == 0) {removeFirst();return true;}	
		if(index == size-1) {removeLast();return true;}	
		int counter = 0;
		Node<T> traversalNode = head;
		Node<T> previousNode = null;
		while(traversalNode.next != null) {
			if(index == counter++) {
				// need to remove this element
				previousNode.next = traversalNode.next;
				traversalNode.data = null;
				traversalNode.next = null;
				traversalNode = null;
				size--;
				return true;
			}
			previousNode = traversalNode;
			traversalNode = traversalNode.next;
		}
		return false;
	}

	public boolean remove(T data) {
		if(null == data) {
			throw new IllegalArgumentException("Invalid Argument Exception "+data);
		}
		Node<T> traversalNode = head;
		if(head.data.equals(data)) {removeFirst(); return true;}
		Node<T> previousNode = null;
		while(traversalNode.next != null) {
			if(data.equals(traversalNode.data)) {
				previousNode.next = traversalNode.next;
				traversalNode.data = null;
				traversalNode.next = null;
				--size;
				return true;
			}else {
				previousNode = traversalNode;
				traversalNode = traversalNode.next;
			}
		}

		if(traversalNode.data.equals(data)) {removeLast(); traversalNode=null;return true;}

		return true;
	}

	public int indexOf(T data) {
		int i=0;
		if(head.data.equals(data)) return i;
		Node<T> traversalNode = head;
		while(traversalNode.next != null) {
			if(traversalNode.data.equals(data)) return i;
			
			traversalNode = traversalNode.next;
			++i;
		}
		if(traversalNode.data.equals(data))
			return i;
		return -1;
	}
	
	public boolean contains(T data) {
		return -1 != indexOf(data);
	}
	
	public void printLinkedList() {
		if(isEmpty()) {
			System.out.println("Empty list");
		}else {
			Node<T> traversalNode = head;
			while(traversalNode != null) {
				System.out.print(traversalNode.data+" ");
				traversalNode = traversalNode.next;
			}
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Node<T> traversalNode = head;
			@Override
			public boolean hasNext() {
				return traversalNode != null;
			}

			@Override
			public T next() {
				T data = traversalNode.data;
				traversalNode = traversalNode.next;
				return data;
			}
		};
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		Node<T> traversalNode = head;
		while(traversalNode.next != null) {
			sb.append(traversalNode.data).append(", ");
			traversalNode = traversalNode.next;
		}
		return sb.append(traversalNode.data).append("]").toString();
	}
	
}
