package darshit.patel.dfs.dynamic.array;

import java.util.Iterator;

public class DynamicArray<T> implements Iterable<T>{
	
	private T[] arr;
	private int length = 0;
	private int capacity = 0;
	private float threshold = 0.75f;
	
	// default constructor to intialize var
	@SuppressWarnings("unchecked")
	public DynamicArray() {
		System.out.println("Calling default constructor to set capacity");
		this.capacity = 10;
		arr = (T[]) new Object[this.capacity];
	}
	
	@SuppressWarnings("unchecked")
	public DynamicArray(int capacity) {
		System.out.println("Setting up capacity to "+capacity);
		if(capacity < 0) {
			throw new IllegalArgumentException("Capacity can not be less than 0");
		}
		this.capacity = capacity;
		arr = (T[]) new Object[this.capacity];
	}
	
	public DynamicArray(int capacity, float threshold) {
		this(capacity);
		if(threshold > 1f)
			throw new IllegalArgumentException("Threshold must be less than 1");
		this.threshold = threshold;
	}
	
	// some functions to expose veriables
	public int size() { return length;}
	public boolean isEmpty() {return 0 == size();}
	public T get(int index) {return arr[index];}
	public void set(int index, T element) {arr[index] = element;}
	
	public void clear() {
		for(int i=0; i<capacity; i++)
			arr[i] = null;
		length = 0;
	}
	
	@SuppressWarnings("unchecked")
	public void add(T element) {
		if(length >= capacity*threshold) {
			System.out.println("Need to resize the array. Increasing arraysize to "+(capacity*2));
			this.capacity = capacity * 2;
			T[] newArray = (T[]) new Object[this.capacity];
			for(int i=0; i<length; i++) {
				newArray[i] = arr[i];
				arr[i] = null;
			}
			arr = null;
			arr = newArray;
		}
		arr[length++] = element;
	}
	
	//removes element from specific index
	public void removeAt(int index) {
		if(index >= this.length || index < 0)
			throw new IndexOutOfBoundsException("Illegal index");
		for(int i = index; i< length; i++) {
			if((i+1) >= length) {
				arr[i] = null;
			}else
				arr[i] = arr[i+1];
		}
		// as element is removed; reduce index by one
		length--;
	}
	
	public boolean remove(T obj) {
		for(int i=0; i< length; i++) {
			if(arr[i].equals(obj)) {
				removeAt(i);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			int index = 0;
			@Override
			public boolean hasNext() {
				return index < length;
			}
			@Override
			public T next() {
				return arr[index++];
			}
		};
	}

	public String toString() {
		if(length == 0)
			return "[]";
		else {
			StringBuilder sb = new StringBuilder("[");
			for(int i=0; i<length-1; i++) {
				sb.append(arr[i]+", ");
			}
			//appending the last element
			sb.append(arr[length-1]+"]");
			return sb.toString();
		}
		
	}
	
}
