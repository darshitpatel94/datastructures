package darshit.patel.dfs.dynamic.array;

import java.util.Iterator;

public class DynamicArrayMain {

	public static void main(String[] args) {
		
		DynamicArray<String> array = new DynamicArray<String>(16, 1f);
		
		array.add("0");
		array.add("1");
		array.add("2");
		array.add("3"); 
		array.add("4"); 
		array.add("5"); 
		array.add("6");
		array.add("7"); 
		array.add("8");
		
		array.removeAt(0);
		array.removeAt(1);
		
		array.remove("4");
		
		Iterator<String> it = array.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		System.out.println(array.toString());
		
	}
	
}
